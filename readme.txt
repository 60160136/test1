เป็นการทำบวก ลบ คูณ หาร โดยกดตัวเลข และเครื่องหมายต่างๆ จากนั้นโปรแกรมจะทำการคำนวน (เครื่องคิดเลข)

ช่วงแรกทำด้วยตัวเอง 
 -ทำเครื่องหมาย + - * / 
 -ทำช่องสำหรับใส่ตัวเลขไว้คำนวณ  
ติดแค่การคลิกเครื่องหมายต่างๆแล้วแสดงเครื่องหมายตามที่คลิก ไม่สามารถทำต่อไปได้
หลังจากนั้นถามคำแนะนำจากเพื่อนและทำตามคำแนะนำของเพื่อน
